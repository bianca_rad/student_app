$(document).ready(function () {

    $('.modal').modal();
    $(".button-collapse").sideNav();


    if (checkParemeterExists('deleted')) {
        Materialize.toast('Deleted', 5000);
    }
    if (checkParemeterExists('added')) {
        Materialize.toast('added', 5000);
    }
    if (checkParemeterExists('edited')) {
        Materialize.toast('edited', 5000);
    }



});
// window.onbeforeunload = function(e) {
//     $('.side-nav').sidenav().close();
//
// };
//-------------------------------get user for every page------------------------

$.ajax({
    type: "GET",
    url: 'http://student.pixel-perfect.ro/getUser.php?token=' + getStorage('token'),
    success: getUser,
    dataType: 'json'
});

function getUser(json) {
    console.log(json);
    if (json.status == 1) { // userul e logat
        $("#user_name").text(json.data.user_name);
        $("#user_email").text(json.data.user_email);
    } else {
        if (window.location.pathname.indexOf('index.html') != -1 || window.location.pathname.indexOf('sign-up.html') != -1) {

        }
        else{
            window.location.href = 'index.html';
        }
    }
}
//-------------------------------------------------------



function checkParemeterExists(parameter) {
    //Get Query String from url
    fullQString = window.location.search.substring(1);

    paramCount = 0;
    queryStringComplete = "?";

    if (fullQString.length > 0) {
        //Split Query String into separate parameters
        paramArray = fullQString.split("&");

        //Loop through params, check if parameter exists.
        for (i = 0; i < paramArray.length; i++) {
            currentParameter = paramArray[i].split("=");
            if (currentParameter[0] == parameter) //Parameter already exists in current url
            {
                return true;
            }
        }
    }

    return false;
}

function logout() {
     removeStorage('token');
    window.location.href = "index.html";
}

function getUrlParam(name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return (results && results[1]) || undefined;
}

function loading(show) {
    if (show) {
        $("#loading").remove();
        $('body').append(
            '<div id="loading">\
                <div class="preloader-wrapper big active">\
                <div class="spinner-layer spinner-blue-only">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
                </div>\
            </div>');
    } else {
        $("#loading").remove();
    }
}


function setStorage(key, value) {
    if (window.localStorage) {
        localStorage.setItem(key, value);
    } else {
        alert('No localstorage :(');
    }
}

function getStorage(key) {
    if (window.localStorage) {
        return localStorage.getItem(key);
    } else {
        alert('No localstorage :(');
        return null;
    }
}

function removeStorage(key) {
    if (window.localStorage) {
        localStorage.removeItem(key);
    } else {
        alert('No localstorage :(');
    }
}


$(document).ready(function () {

    //  $('body').css('display', 'none');

    $('body').fadeIn(400);



    $('.menu-list-bottom a').click(function (event) {

        event.preventDefault();

        newLocation = this.href;

        $('body').fadeOut(200, newpage);

    });



    function newpage() {

        window.location = newLocation;

    }



});

